'use strict';

module.exports = function(grunt){
	require('time-grunt')(grunt);
	require('jit-grunt')(grunt);
	grunt.initConfig({
		sass:{
			dist:{
				file:{
					'css/styles.css':'css/styles.css'
				}

			}
		},
		watch:{
			file:'css/*.scss',
			task:['sass']
		},
		 browserSync:{
			dev:{
				bsFiles:{
					src:[
					'css/*.scss','*.html','JS/*.js',]
				},
				options: {
                    watchTask: true,
                    server: {
                        baseDir: "./"
                    }
                }
				

			}
		}
	});
	grunt.registerTask('css',['sass']);
	grunt.registerTask('default', ['browserSync', 'watch']);
};